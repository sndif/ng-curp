angular
  .module 'ngCurp', []
  .factory 'curpMask', [
    'curpEntidades'
    ( entidades ) ->
      {
        mask : 'a{4}YYmMdDGEEa{3}V9'
        definitions :
          'a' :
            validator : '[a-zA-Z]'
            casing : 'upper'
          'Y' :
            validator : "[0-9]"
          'm' :
            validator : "0|1"
          'M' :
            validator : ( c, m, p, s, o ) ->
              return false if not /\d/.test( c )
              c   = parseInt c
              dec = parseInt m.buffer[ p - 1 ]
              if dec is 1 and c > 2
                return false
              true
          'd' :
            validator : "[0-3]"
          'D' :
            validator : ( c, m, p, s, o ) ->
              return false if not /\d/.test( c )
              c   = parseInt c
              dec = parseInt m.buffer[ p - 1 ]
              if dec is 3 and c > 1
                return false
              true
          'G' :
            validator : "[hmHM]"
            casing : 'upper'
          'E' :
            validator : ( c, m, p, s, o ) ->
              return false if not /[a-zA-Z]/.test( c )
              c = c.toUpperCase()
              if p is 11
                flag = false
                for k, v of entidades
                  flag = true if k.charAt( 0 ) is c
              else
                prev = m.buffer[ 11 ]
                entidad = prev + c
                flag = false
                for k, v of entidades
                  flag = true if k is entidad
              flag
            casing : 'upper'
          'V' :
            validator : '[a-z0-9]'
            casing : 'upper'
      }
  ]

  .factory 'curpTranslateCharacters', ->
    {
      'Á': 'A'
      'É': 'E'
      'Í': 'I'
      'Ó': 'O'
      'Ú': 'U'
      'Ü': 'U'
    }

  .factory 'curpEscapeCharacters', ->
    [
      'Ñ'
      '/'
      '-'
      '.'
      '\''
    ]

  .factory 'curpEscapeNames', ->
    [
      'MARIA'
      'MA.'
      'MA'
      'M.'
      'M'
      'JOSE'
      'J.'
      'J'
    ]

  .factory 'curpEscapeLinkers', ->
    [
      'DA'
      'DAS'
      'DE'
      'DEL'
      'DER'
      'DI'
      'DIE'
      'DD'
      'Y'
      'EL'
      'LA'
      'LOS'
      'LAS'
      'LE'
      'LES'
      'MAC'
      'MC'
      'VAN'
      'VON'
    ]

  .factory 'curpEscapeWords', ->
    [
      'BACA'
      'BAKA'
      'BUEI'
      'BUEY'
      'CACA'
      'CACO'
      'CAGA'
      'CAGO'
      'CAKA'
      'CAKO'
      'COGE'
      'COGI'
      'COJA'
      'COJE'
      'COJI'
      'COJO'
      'COLA'
      'CULO'
      'FALO'
      'FETO'
      'GETA'
      'GUEI'
      'GUEY'
      'JETA'
      'JOTO'
      'KACA'
      'KACO'
      'KAGA'
      'KAGO'
      'KAKA'
      'KAKO'
      'KOGE'
      'KOGI'
      'KOJA'
      'KOJE'
      'KOJI'
      'KOJO'
      'KOLA'
      'KULO'
      'LILO'
      'LOCA'
      'LOCO'
      'LOKA'
      'LOKO'
      'MAME'
      'MAMO'
      'MEAR'
      'MEAS'
      'MEON'
      'MIAR'
      'MION'
      'MOCO'
      'MOKO'
      'MULA'
      'MULO'
      'NACA'
      'NAKA'
      'NACO'
      'NAKO'
      'PEDA'
      'PEDO'
      'PENE'
      'PIPI'
      'PITO'
      'POTO'
      'PUTA'
      'PUTO'
      'QULO'
      'RATA'
      'ROBA'
      'ROBE'
      'ROBO'
      'RUIN'
      'SENO'
      'TETA'
      'VACA'
      'VAGA'
      'VAGO'
      'VAKA'
      'VUEI'
      'VUEY'
      'WUEI'
      'WUEY'
    ]

  .factory 'curpEntidades', ->
    {
      AS : 'AGUASCALIENTES'
      BC : 'BAJA CALIFORNIA'
      BS : 'BAJA CALIFORNIA SUR'
      CC : 'CAMPECHE'
      CL : 'COAHUILA DE ZARAGOZA'
      CM : 'COLIMA'
      CS : 'CHIAPAS'
      CH : 'CHIHUAHUA'
      DF : 'DISTRITO FEDERAL'
      DG : 'DURANGO'
      GT : 'GUANAJUATO'
      GR : 'GUERRERO'
      HG : 'HIDALGO'
      JC : 'JALISCO'
      MC : 'MEXICO'
      MN : 'MICHOACAN DE OCAMPO'
      MS : 'MORELOS'
      NT : 'NAYARIT'
      NL : 'NUEVO LEON'
      OC : 'OAXACA'
      PL : 'PUEBLA'
      QT : 'QUERETARO DE ARTEAGA'
      QR : 'QUINTANA ROO'
      SP : 'SAN LUIS POTOSI'
      SL : 'SINALOA'
      SR : 'SONORA'
      TC : 'TABASCO'
      TS : 'TAMAULIPAS'
      TL : 'TLAXCALA'
      VZ : 'VERACRUZ'
      YN : 'YUCATAN'
      ZS : 'ZACATECAS'
      NE : 'NACIDO EN EL EXTRANJERO'
    }

  .directive 'ngCurp', [
    'curpMask'
    'curpEscapeLinkers'
    'curpTranslateCharacters'
    'curpEscapeCharacters'
    'curpEscapeNames'
    'curpEscapeWords'
    ( curpMask, linkers, translateCharacters, escapeCharacters, escapeNames, escapeWords ) ->
      {
        restrict : 'E'
        template : '<div class="is-control"><input type="text" size="1" ng-model="ngModel" input-mask="\'mask\'" mask-option="curpMask"/></div>'
        replace : true
        require : [
          'ngModel'
        ]
        scope :
          ngModel : '='
          cuNombre : '='
          cuPrimerApellido : '='
          cuSegundoApellido : '='
          cuFechaNacimiento : '='
          cuGenero : '='
          cuEntidad : '='
          cuFechaFormat : '@'
          cuMujer : '@'
          cuHombre : '@'

        link : ( $scope, $elem, attrs, ctrls ) ->

          # mask
          $scope.curpMask = curpMask

          # clean text
          cleanText = ( text ) ->
            text = text.toUpperCase()

            text = text.split( /\s+/ ).filter( ( word ) -> linkers.indexOf( word ) is -1 ).join ' '

            for k, v of translateCharacters
              text = text.replace k, v

            for char in escapeCharacters
              text = text.replace char, 'X'

            text.trim()

          # get vocal in a word
          getVocal = ( text ) ->
            vocals = text.match /[AEIOU]/gi
            return vocals[0] if vocals?.length > 0
            'X'

          # clean names
          cleanNames = ( text ) ->
            text
              .split( /\s+/g ).filter ( name ) ->
                escapeNames.indexOf( name ) is -1
              .join ' '

          escapeBadWords = ->
            word = []
            word.push scrapPrimerApellido() || 'XX'
            word.push scrapSegundoApellido() || 'X'
            word.push scrapNombre() || 'X'
            word = word.join ''

            return word.slice( 0,1 ) + 'X' + word.slice( 2 ) if escapeWords.indexOf( word ) isnt -1

            word

          # scrap primer apellido
          scrapPrimerApellido = ->
            return if not $scope.cuPrimerApellido
            value = cleanText $scope.cuPrimerApellido
            vocal = getVocal value.substr 1
            value.slice( 0, 1 ) + vocal

          # scrap segundo apellido
          scrapSegundoApellido = ->
            return if not $scope.cuSegundoApellido
            value = cleanText $scope.cuSegundoApellido
            value.slice 0, 1

          # scrap nombre
          scrapNombre = ->
            return if not $scope.cuNombre
            value = cleanText $scope.cuNombre
            cleanNames( value ).slice 0, 1

          # process born date
          scrapFechaNacimiento = ->
            return if not $scope.cuFechaNacimiento
            fecha = moment $scope.cuFechaNacimiento, $scope.cuFechaFormat
            fecha.format 'YYMMDD'

          # genero
          scrapGenero = ->
            return if not $scope.cuGenero

            m = 'MUJER'
            m = $scope.cuMujer if $scope.cuMujer

            h = 'HOMBRE'
            h = $scope.cuHombre if $scope.cuHombre

            return 'M' if $scope.cuGenero is m

            'H'

          getConsonante = ( input ) ->
            consonants = input.match /[^AEIOU]/gi
            return consonants[ 0 ] if consonants?.length > 0
            'X'

          scrapPrimerApellidoConsonante = ->
            return if not $scope.cuPrimerApellido
            value = cleanText $scope.cuPrimerApellido
            getConsonante value.slice 1

          scrapSegundoApellidoConsonante = ->
            return if not $scope.cuSegundoApellido
            value = cleanText $scope.cuSegundoApellido
            getConsonante value.slice 1

          scrapNombreConsonante = ->
            return if not $scope.cuNombre
            value = cleanText $scope.cuNombre
            getConsonante value.slice 1

          scrapConsonantes = ->
            consonantes = []
            consonantes.push scrapPrimerApellidoConsonante() || 'X'
            consonantes.push scrapSegundoApellidoConsonante() || 'X'
            consonantes.push scrapNombreConsonante() || 'X'
            consonantes.join ''

          generate = ->
            if $scope.ngModel is undefined
              $scope.ngModel = '_'.repeat 18

            partterns = [
              /[A-Z]{1,4}/
              /\d{1,6}/
              /[HM]/
              /[A-Z]{2}/
              /[A-Z]{3}/
              /[A-Z0-9]/
              /\d/
            ]

            re = new RegExp '(' + partterns.map( ( item ) -> return item.source ).join( ')?(' ) + ')?'

            parts = $scope.ngModel.match re
            parts.shift()

            # primer parte
            parts[ 0 ] = escapeBadWords()

            # fecha de nacimiento
            fechaNacimiento = scrapFechaNacimiento()
            parts[ 1 ] = fechaNacimiento if fechaNacimiento

            # genero
            genero = scrapGenero()
            parts[ 2 ] = genero if genero

            # entidad de nacimiento

            # consonantes
            parts[ 4 ] = scrapConsonantes() if parts[ 3 ] isnt undefined

            parts.join ''

          # watchers
          $scope.$watch 'cuNombre', ->
            return if not $scope.cuNombre
            $scope.ngModel = generate()
          , true

          $scope.$watch 'cuPrimerApellido', ->
            return if not $scope.cuPrimerApellido
            $scope.ngModel = generate()
          , true

          $scope.$watch 'cuSegundoApellido', ->
            return if not $scope.cuSegundoApellido
            $scope.ngModel = generate()
          , true

          $scope.$watch 'cuFechaNacimiento', ->
            return if not $scope.cuFechaNacimiento
            $scope.ngModel = generate()
          , true

          $scope.$watch 'cuGenero', ->
            return if not $scope.cuGenero
            $scope.ngModel = generate()
          , true

          $scope.$watch 'ngModel', ->
            $scope.ngModel = generate()
          , true

      }
  ]
